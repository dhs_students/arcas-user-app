import 'package:flutter/material.dart';

class AboutArcas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('About Arcas')
        ),
        body: ListView(
          children: <Widget>[
            Center(
              child: Container(
                padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
                child: Text("<About Arcas Information here>")
              )
            )
          ]
        )
      )
    );
  }
}