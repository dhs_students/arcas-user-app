import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'AppScaffold.dart';
import 'FirebaseHelper.dart';

import 'Themes.dart' as myThemes;
import 'SharedPrefs.dart';
import 'ArcasGlobals.dart';

void main() {
	// So SchedulerBinding works fine
	WidgetsFlutterBinding.ensureInitialized();

	// Run the app
	runApp(ArcasApp());
}

class ArcasApp extends StatelessWidget {
	Future<void> initAll() async{
		// Set our initial preferences and the firebase stuff
		await prefs.init();
		usersFirstVisit = prefs.getFirstVisit();
		prefs.setFirstVisit(false);

		await firebaseHelper.init();
	}

	@override
	Widget build(BuildContext context) {
		return FutureBuilder(
				future: initAll(),
				builder: (context, snapshot) {
					if(snapshot.connectionState == ConnectionState.done) {
						return GetMaterialApp(
								title: 'Arcas',
								debugShowCheckedModeBanner: false,
								// Modify the theme/darkTheme logic to depend on the darkMode flag
								theme: prefs.getDarkMode() ? myThemes.themeDark : myThemes.themeLight,
								home: AppScaffold()
						);
					}
					return Center(child: Text("Loading...", textDirection: TextDirection.ltr));
				}
		);
	}
}