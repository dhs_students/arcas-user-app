import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

enum AgeOptions { AU18, A1825, A250, NA }
enum GenderOptions { M, F, X, NA }

Map<List<String>, AgeOptions> _ageMap = {
  ["U18", "0 - 17"] : AgeOptions.AU18,
  ["1825", "18 - 25"] : AgeOptions.A1825,
  ["250", "25+"] : AgeOptions.A250,
  ["NA"] : AgeOptions.NA
};

Map<List<String>, GenderOptions> _genderMap = {
  ["M","Male"] : GenderOptions.M,
  ["F", "Female"] : GenderOptions.F,
  ["X"] : GenderOptions.X,
  ["NA"] : GenderOptions.NA,
};

AgeOptions parseAge(String age) {
  return _ageMap.entries.firstWhere((element) => element.key.contains(age)).value;
}

GenderOptions parseGender(String gender) {
  return _genderMap.entries.firstWhere((element) => element.key.contains(gender)).value;
}


//all of the fields in this class should only be getters, no setting allowed
class ArcasConstants {

  static final Map<String, List<IconData>> iconNames = {
    "Charging Point" : [Icons.power, Icons.power_outlined],
    "Food" : [Icons.restaurant, Icons.restaurant_sharp],
    "Clock" : [Icons.schedule, Icons.schedule_outlined],
    "Health Service" : [Icons.healing, Icons.healing_sharp],
    "WiFi" : [Icons.wifi,Icons.wifi_rounded],
    "Community Centre" : [Icons.local_library,Icons.local_library_outlined],
    "Clothing" : [Icons.backpack, Icons.backpack_outlined],
    "Emergency Accommodation" : [Icons.house_siding,Icons.house_siding_sharp],
    "Emergency Relief" : [Icons.rowing,Icons.rowing_sharp],
    "Long Term Accommodation" : [Icons.home,Icons.home_outlined],
    "Other" : [Icons.settings_ethernet,Icons.settings_ethernet_sharp],
    "Laundry" : [Icons.local_laundry_service,Icons.local_laundry_service_outlined],
    "Shower" : [Icons.waves,Icons.waves_sharp],
    "Toilet" : [Icons.airline_seat_recline_normal,Icons.airline_seat_recline_normal_rounded],
    "Transport" : [Icons.commute,Icons.commute_sharp],
    "Water" : [Icons.opacity,Icons.opacity_rounded]

  };

  static final Map locationNames = {
    "ChargingPoint" : "Charging Point",
    "Clothing" : "Clothing",
    "CommunityCentre" : "Community Centre",
    "EmergencyAccommodation" : "Emergency Accommodation",
    "EmergencyRelief" : "Emergency Relief",
    "Food" : "Food",
    "HealthService" : "Health Service",
    "Laundry" : "Laundry",
    "LongTermAccommodation" : "Long Term Accommodation",
    "Other" : "Other",
    "Shower" : "Shower",
    "Toilet" : "Toilet",
    "Transport" : "Transport",
    "Water" : "Water",
    "WiFi" : "WiFi"
  };


  static String get date => DateFormat('EEEE').format(DateTime.now());

  //categories are defined as they are in firebase (no spaces)
  static final categoryPriority = ["Food","Water","Clothing","Toilet","Shower","Health Service","Long Term Accommodation","Emergency Accommodation","Emergency Relief",
    "Transport","WiFi","Community Centre","Laundry","Charging Point","Other"];

  static final capacities = ["LOW","MEDIUM","HIGH","N/A"];
  static final Map<String, int> capacityNumber = {"LOW" : 0, "MEDIUM" : 1, "FULL" : 2, "NA" : 3};
}

