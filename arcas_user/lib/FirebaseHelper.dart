import 'dart:collection';

import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:mapbox_gl/mapbox_gl.dart' as Mapbox;
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

import 'SearchStateManager.dart';
import 'ArcasFilter.dart';
import 'ArcasConstants.dart';
import 'ArcasLocationPage.dart';

final FirebaseHelper firebaseHelper = FirebaseHelper();

class FirebaseHelper {
  static Future<FirebaseApp> _initialization = Firebase.initializeApp();
  FirebaseFirestore firestore;
  FirebaseFunctions functions;

  Future init() async {
    await _initialization;
    firestore = FirebaseFirestore.instance;
    functions = FirebaseFunctions.instance;
  }


  CollectionReference waitForData(coll) {
    return firestore.collection(coll);
  }

  Future<DocumentSnapshot> getDoc(String collectionName, String documentID) {
    return firestore.collection(collectionName).doc(documentID).get();
  }

  Future<List<DocumentSnapshot>> getMatchingDocuments(String collectionName, List<String> documentIDs) async {
    List<DocumentSnapshot> results = List();
    for(String id in documentIDs) {
      results.add(await firestore.collection(collectionName).doc(id).get());
    }
    return results;
  }

  Future<QuerySnapshot> getFilteredEqual(coll, filter, filterParam) {
    return FirebaseFirestore.instance.collection(coll).where(filter, isEqualTo: filterParam).get();
  }

  Future<QuerySnapshot> getGeneralSnapshot(coll) {
    return waitForData(coll).get();
  }

  Stream<QuerySnapshot> getServicesFromId(String id) {
    DocumentReference dr = FirebaseFirestore.instance.collection('provider-location').doc(id);
    return FirebaseFirestore.instance.collection("provider-services")
        .where('location', isEqualTo: dr)
        .snapshots();
  }

  void sendFeedback(int ratingStars, String ratingComeBack, String ratingFeedback, String id) {
    DocumentReference location = FirebaseFirestore.instance.collection('provider-location').doc(id);
    Map<String, dynamic> data = {
      'createdAt' : Timestamp.now(),
      'description' : ratingFeedback == "" ? "No Description Provided" : ratingFeedback,
      'location' : location,
      'rating' : ratingStars,
      'updatedAt' : Timestamp.now(),
      'wouldReturn' : ratingComeBack
    };
    FirebaseFirestore.instance.collection("provider-feedback").add(data);
  }

}

// Listened to by:
// ArcasMap: for capacity changes
// ArcasLocationPage: for capacity changes
class LocationPoint extends ChangeNotifier {
  Map latLng;
  bool isBlank; // Used for objects where not all vars have been set. Can be used to easily avoid rendering stuff
  bool isActive = false; //Used to check if the search state contains this location.
  String name;
  String address;
  _OpeningHours weekInfo; // Contains the info for all days of the week
  String description;
  String phoneNumber;
  String website;
  String id;
  int averageCapacity = 3;  // Average capacity rounded: Defaults to NA if not found.
  int minimumCapacity = 3; //Minimum: Defaults to NA
  String image = "";
  Mapbox.Symbol symbol;
  SplayTreeSet<ArcasService> serviceList;
  Map locationInfo;
  List<String> categories = [];
  Mapbox.SymbolOptions symbolOptions;
  StreamSubscription serviceStream;

  void Function() updateSymbol;

  LocationPoint.blank() {
    isBlank = true;
    latLng = Map();
    name = 'Blank';
    address = 'Blank';
    categories = ['Blank'];
  }

  factory LocationPoint.fromDocumentSnapshot(DocumentSnapshot doc) {
    Map transformed = {
      "locationData": doc.data(),
      "ref":  doc.id,
    };
    var lp = LocationPoint(transformed);
    firebaseHelper.getServicesFromId(lp.id).listen((querySnapshot) {
      lp.updateServicesList(querySnapshot.docChanges,true);
    });
    return lp;
  }

  LocationPoint(Map providerLocation) {
    isBlank = false;
    Map data = providerLocation["locationData"]["d"];
    id = providerLocation["ref"];
    locationInfo = data;
    latLng = data['latLng'];
    name = data['title'];
    address = data['address']['formattedAddress'];
    if(data["orgAvatar"] != null) {
      image = data["orgAvatar"];
    }
    categories = [];  // Will be populated later
    weekInfo = _OpeningHours(data['openingHours']);
    description = data['description'];
    phoneNumber = data['phone'];
    website = buildWebsite(data['website']);
    serviceList = new SplayTreeSet<ArcasService>((a,b) => a.priority.compareTo(b.priority));
  }

  bool closesSoon(DateTime time){
    List<String> res = nextTime(time, true);  // Gets the end time

    if(res.length != 2){  // Should never happen
      debugPrint("Incorrect behaviour, should never happen");
      return true;
    }

    // If a place is never open
    if(res[0] == "" || res[1] == ""){
      return false;
    }

    DateTime endTime = hourColonMinuteToDateTime(res[1], time);
    if(endTime.difference(time).inHours < 1){ // If time is more than end time somehow, this will be negative
      return true;
    }
    return false;
  }

  // Returns an array of two strings (Day + open time)
  // If we are currently in a time period, then return the current open time
  // If returnEndTime is true we return the end time, else we return the start time
  List<String> nextTime(DateTime time, bool returnEndTime){
    // Check the end times, not the start times
    String day = DateFormat('EEEE').format(time);

    DateTime newTime;
    String newDay = DateFormat('EEEE').format(time);
    DateTime endTime;
    int j = -1;  // Start checking on the current day
    while(true){
      j++;

      newTime = time.add(new Duration(days: j));
      newDay = DateFormat('EEEE').format(newTime);

      // We didn't find the day and are now back to the beginning
      if(newDay == day && j != 0){
        break;
      }

      // They aren't open on this day
      if(weekInfo.day(newDay).isOpen == false){
        continue;
      }

      for(var i = 0; i < weekInfo.day(newDay).times.length; i++){
        endTime = hourColonMinuteToDateTime(weekInfo.day(newDay).times[i].endTime, newTime);

        if(newTime.isBefore(endTime)) {
          // Return the day and either the start or end time as specified
          OpeningHoursDayHours time = weekInfo.day(newDay).times[i];
          return [newDay.toString(), returnEndTime? time.endTime : time.startTime];
        }
      }
    }

    return ["", ""];
  }

  // Checks if its open at the specified local time
  bool serviceOpen(DateTime time){
    String day = DateFormat('EEEE').format(time);

    // If they specified they aren't open today, end early
    if(this.weekInfo.day(day).isOpen == false){
      return false;
    }

    // Check if its in a current time
    bool isOpen = false;

    DateTime startTime;
    DateTime endTime;
    for(var i = 0; i < this.weekInfo.day(day).times.length; i++){
      startTime = hourColonMinuteToDateTime(this.weekInfo.day(day).times[i].startTime, time);
      endTime = hourColonMinuteToDateTime(this.weekInfo.day(day).times[i].endTime, time);

      if(time.isAfter(startTime) && time.isBefore(endTime)){
        isOpen = true;
        break;
      }
    }

    return isOpen;
  }

  // Returns a string of all the available times for a given day
  String dayAvailableTimes(DateTime time){
    var times = weekInfo.day(DateFormat('EEEE').format(time)).times;
    if(times.length == 0){
      return "";
    }
    String output = "";
    for(var i = 0; i < times.length; i++){
      output += timeToAmPm(times[i].startTime) + " - " + timeToAmPm(times[i].endTime);
      if(i != times.length - 1){
        output += " ; ";
      }
    }

    return output;
  }

  //updates services
  void updateServicesList(List<DocumentChange> services, [bool skipLoad = false]) {
    String day = DateFormat('EEEE').format(new DateTime.now());
    categories = [];
    services.forEach((service) async {
      Map<String, dynamic> data = service.doc.data();
      ArcasService as = ArcasService(this, data, service.doc.id);
      serviceList.remove(as);
      serviceList.add(as);
    });
    for(ArcasService service in serviceList) {
      if(service.enabled == true && service.openingHours.day(day).isOpen == true) {
        categories.add(service.category);
      }
    }
    updateCapacities();
    if(!skipLoad && !isActive) {
      SearchStateManager().processFilter(this);
    }
    notifyListeners();
  }

  void updateCapacities() {
    filterStorage.getAverageAndMinimumCapacity(this);
  }

  void cancelStream() {
    if(serviceStream != null) {
      serviceStream.cancel();
    }
  }

  Text buildCategoriesList() {
    return Text(
        categories.isNotEmpty
            ? categories.join(", ")
            : "No services offered today.",
        maxLines: 1,
        overflow: TextOverflow.ellipsis
    );
  }

  Text buildOpeningTimes() {
    DateTime currentTime = DateTime.now();
    bool openStatus = serviceOpen(currentTime);
    bool closingSoon = closesSoon(currentTime);
    return Text.rich(
        TextSpan(
            children: [
              TextSpan(
                  text: openStatus ? (closingSoon ? "Closes Soon" : "Open") : "Closed",
                  style: TextStyle(
                    color: openStatus ? (closingSoon ? Colors.orange[700] : Colors.green) : Colors.red,
                  )
              ),
              TextSpan(
                  text: " | " + (openStatus
                      ? dayAvailableTimes(currentTime)
                      : nextOpenTimeToString(nextTime(currentTime, false)))
              )
            ]
        ),
        overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.left,
        maxLines: 1
    );
  }

  Text buildDescription() {
    return Text(
        description,
        maxLines: 1,
        overflow: TextOverflow.ellipsis
    );
  }

  ListTile buildListTile(context) {
    return ListTile(
        title: Text(
            name
        ),
        subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buildCategoriesList(),
              buildOpeningTimes(),
              buildDescription()
            ]
        ),
        leading: Padding(
          padding: EdgeInsets.all(8.0),
          child: Icon(Icons.place),
        ),
        onTap: () => Navigator.of(context).push(ArcasLocationPage.createRoute(this))
    );
  }

  //builds the correct string for the website for url_launcher to use.
  String buildWebsite(String site) {
    if(site == null || site == "") {
      return "";
    }
    if(site.startsWith("http")) {
      return site;
    }
    return Uri.https(site, "").toString();
  }

}

// THIS WILL BREAK IF SERVICE GOES OVER TO THE NEXT DAY
// To fix this we'd need to detect that and also increment the day, month and
// possibly year options for startTime and endTime
// Would mess up if the service *ends* on the next day (Including midnight I think)

DateTime hourColonMinuteToDateTime(String s, [DateTime time]){
  time ??= DateTime.now();
  if(s == null) {
    throw "hourColonMinuteToDateTime(), No string provided";
  }
  if(s == "") {
    throw "hourColonMinuteToDateTime() called with empty string";
  }

  var info = s.split(':');
  if(info.length != 2){
    throw "hourColonMinuteToDateTime() string in wrong format";
  }
  return DateTime(time.year, time.month, time.day, int.parse(info[0]), int.parse(info[1]));
}

// Converts the result of "nextOpenTime" to a nicely formatted string
String nextOpenTimeToString(List<String> s) {
  if(s.length != 2){
    return "";
  }

  // If a place is never open, nextTime() will return a list of empty strings
  if(s[0] == "" || s[1] == ""){
    return "Never opens";
  }

  // Shorten "s[0]" (aka Wednesday) to something like "Wed"
  // Mon, Tues, Wed, Thur, Fri, Sat, Sun
  String day = s[0];
  switch(day){
    case "Monday":
      day = "Mon";
      break;
    case "Tuesday":
      day = "Tues";
      break;
    case "Wednesday":
      day = "Wed";
      break;
    case "Thursday":
      day = "Thur";
      break;
    case "Friday":
      day = "Fri";
      break;
    case "Saturday":
      day = "Sat";
      break;
    case "Sunday":
      day = "Sun";
      break;
    default:
      throw "Invalid Day for nextOpenTimeToString()";
  }

  return "Opens " + day + " at " + timeToAmPm(s[1]);
}

String timeToAmPm(String date) {
  if(date == ""){return date;}  // Just incase this somehow gets in
  final DateFormat formatter = DateFormat('jm');
  DateTime tempDate = new DateFormat("Hm").parse(date);
  return formatter.format(tempDate);
}

//grabs all relevant data from services
class ArcasService {
  String id;
  LocationPoint parentLocation;
  bool enabled;
  List<AgeOptions> ages;
  List<GenderOptions> genders;
  //category is the human-readable form of the input category. Priority is a number from 0 to x that determines priority of category, 0 being highest
  String category;
  int priority;
  //capacity is LOW,MEDIUM,FULL,NA. capacityNumber is that capacity in number form (0,1,2,3)
  String capacity;
  int capacityNumber;
  _OpeningHours openingHours;

  ArcasService(LocationPoint location, Map service, String _id) {
    id = _id;
    parentLocation = location;
    enabled = service['isEnabled'];
    ages = List<AgeOptions>.from(service['age'].map((e) => parseAge(e)));
    genders = List<GenderOptions>.from(service['gender'].map((e) => parseGender(e)));
    category = ArcasConstants.locationNames[service['category']];
    priority = ArcasConstants.categoryPriority.indexOf(category);
    capacity = service['capacity'];
    capacityNumber = ArcasConstants.capacityNumber[service['capacity']];
    openingHours = _OpeningHours(service['openingHours']);

  }

  @override
  bool operator ==(dynamic other) {
    return id == other.id;
  }

  @override
  int get hashCode => id.hashCode;

}

//used in both LocationPoint and Services as their times have the same structure
//has a OpeningHoursDay / OpeningHoursDayHours to assist in data strcuture.
class _OpeningHours {
  OpeningHoursDay monday;
  OpeningHoursDay tuesday;
  OpeningHoursDay wednesday;
  OpeningHoursDay thursday;
  OpeningHoursDay friday;
  OpeningHoursDay saturday;
  OpeningHoursDay sunday;

  _OpeningHours(Map openHours) {
    monday = OpeningHoursDay(openHours['Monday']);
    tuesday = OpeningHoursDay(openHours['Tuesday']);
    wednesday = OpeningHoursDay(openHours['Wednesday']);
    thursday = OpeningHoursDay(openHours['Thursday']);
    friday = OpeningHoursDay(openHours['Friday']);
    saturday = OpeningHoursDay(openHours['Saturday']);
    sunday = OpeningHoursDay(openHours['Sunday']);
  }

  OpeningHoursDay day(String dayString) {
    switch (dayString) {
      case 'Monday': return monday;
      case 'Tuesday': return tuesday;
      case 'Wednesday': return wednesday;
      case 'Thursday' : return thursday;
      case 'Friday' : return friday;
      case 'Saturday' : return saturday;
      case 'Sunday' : return sunday;
      default: throw("No valid date");
    }
  }

}

class OpeningHoursDay {
  bool isOpen;
  List<OpeningHoursDayHours> times;

  OpeningHoursDay(Map day) {
    times = [];
    isOpen = day['isOpen'];
    for(Map time in day['times']) {
      times.add(OpeningHoursDayHours(time));
    }
  }
}

class OpeningHoursDayHours {
  bool hasError;
  String startTime;
  String endTime;

  OpeningHoursDayHours(Map hours) {
    startTime = hours['startTime'];
    endTime = hours['endTime'];
    hasError = hours['hasError'];
  }
}