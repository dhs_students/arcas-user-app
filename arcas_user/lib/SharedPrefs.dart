import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'dart:math';

import 'package:flutter/scheduler.dart';

final SharedPrefs prefs = SharedPrefs();

class SharedPrefs {
  List<String> recentSearchesDefault = [];
  List<String> savedLocationIDsDefault = [];
  String genderDefault = 'NA';
  String ageDefault = 'NA';
  bool darkModeDefault = false; // Should pretty much go unused
  bool firstVisitDefault = true;  // Is only ever true on your first visit. Used to bring up a screen for the user to set their defaults

  SharedPreferences sharedPrefs;

  // Creates the variables if not present
  init() async {
    this.sharedPrefs = await SharedPreferences.getInstance();

    if(!this.sharedPrefs.containsKey('recentSearches')) {
      await setRecentSearches(recentSearchesDefault);
    }
    if(!this.sharedPrefs.containsKey('savedLocationIDs')) {
      await setSavedLocationIDs(savedLocationIDsDefault);
    }
    if(!this.sharedPrefs.containsKey('gender')) {
      await setGender(genderDefault);
    }
    if(!this.sharedPrefs.containsKey('age')) {
      await setAge(ageDefault);
    }
    if(!this.sharedPrefs.containsKey('darkMode')) {
      // Check if system has Dark Mode enabled
      var brightness = SchedulerBinding.instance.window.platformBrightness;
      await setDarkMode(brightness == Brightness.dark);
    }
    if(!this.sharedPrefs.containsKey('firstVisit')) {
      await setFirstVisit(firstVisitDefault);
    }
  }

  // Getters

  List<String> getRecentSearches() {
    return this.sharedPrefs.getStringList('recentSearches') ?? recentSearchesDefault;
  }

  List<String> getSavedLocationIDs() {
    return this.sharedPrefs.getStringList('savedLocationIDs') ?? savedLocationIDsDefault;
  }

  String getGender() {
    return this.sharedPrefs.getString('gender') ?? genderDefault;
  }

  String getAge() {
    return this.sharedPrefs.getString('age') ?? ageDefault;
  }

  bool getDarkMode() {
    return this.sharedPrefs.getBool('darkMode') ?? darkModeDefault;
  }

  bool getFirstVisit() {
    return this.sharedPrefs.getBool('firstVisit') ?? firstVisitDefault;
  }

  // Setters

  Future<void> setRecentSearches(List<String> val) async {
    //Use .sublist() to limit the number of saved searches
    int limit = 15;
    await this.sharedPrefs.setStringList('recentSearches', val.sublist(0, min(val.length, limit)));
  }

  Future<void> setSavedLocationIDs(List<String> val) async {
    await this.sharedPrefs.setStringList('savedLocationIDs', val);
  }

  Future<void> setGender(String val) async {
    await this.sharedPrefs.setString('gender', val);
  }

  Future<void> setAge(String val) async {
    await this.sharedPrefs.setString('age', val);
  }

  Future<void> setDarkMode(bool val) async {
    await this.sharedPrefs.setBool('darkMode', val);
  }

  Future<void> setFirstVisit(bool val) async {
    await this.sharedPrefs.setBool('firstVisit', val);
  }
}
